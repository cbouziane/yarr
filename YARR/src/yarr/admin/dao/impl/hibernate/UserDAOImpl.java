package yarr.admin.dao.impl.hibernate;


import yarr.admin.dao.UserDAO;
import yarr.admin.domain.User;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

public class UserDAOImpl implements UserDAO
{
	// Hibernate utility class.
	private final HibernateUtil hibernateUtil;
	
	/**
	 * Class constructor.
	 * @param hibernateUtil
	 */
	public UserDAOImpl(HibernateUtil hibernateUtil)
	{
		this.hibernateUtil = hibernateUtil;
	}

	@Override
	public User getUserByEmail(String email)
	{
		Session session = hibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		Query query = session.createQuery("from User where email = ?");
		query.setString(0, email);
		User user = (User) query.uniqueResult();
		tx.commit();
		return user;
	}

	@Override
	public void create(User user)
	{
		Session session = hibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.persist(user);
		tx.commit();
	}

	/*
	 * (non-Javadoc)
	 * @see nl.hanze.funda.admin.dao.UserDAO#update(nl.hanze.funda.admin.domain.User)
	 */
	@Override
	public void update(User user)
	{
		Session session = hibernateUtil.getSessionFactory().getCurrentSession();
		Transaction tx = session.beginTransaction();
		session.update(user);
		tx.commit();
	}

}
