package yarr.admin.dao.impl.hibernate;

import yarr.admin.dao.UserDAO;
import yarr.admin.dao.impl.hibernate.HibernateUtil;
import yarr.admin.dao.impl.hibernate.UserDAOImpl;
import yarr.admin.dao.*;

public class DAOFacadeImpl implements DAOFacade
{
	private HibernateUtil hibernateUtil;
	private UserDAO userDAO;

	/**
	 * Class constructor.
	 */
	public DAOFacadeImpl()
	{
		// Initialize the hibernate utility class.
		hibernateUtil = new HibernateUtil();
	}

	@Override
	public UserDAO getUserDAO()
	{
		if (userDAO == null)
			// Initialize the DAO on demand.
			userDAO = new UserDAOImpl(hibernateUtil);
		return userDAO;
	}
}
