package yarr.admin.dao;

import yarr.admin.domain.User;

public interface UserDAO
{
	/**
	 * Get a username by name.
	 * @param username
	 * @return
	 */
	public User getUserByEmail(String email);
	
	/**
	 * Create a new user entry.
	 * @param user
	 */
	public void create(User user);
	
	/**
	 * Update a user.
	 * @param user
	 */
	public void update(User user);
}
