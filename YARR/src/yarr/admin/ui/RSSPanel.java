package yarr.admin.ui;

import javax.swing.*;
import javax.swing.text.JTextComponent;

import yarr.admin.logic.Logic;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;



@SuppressWarnings("serial")
public class RSSPanel extends JPanel {
	private final Logic logic;
	private JLabel rssCleanLabel;
	private JButton rssCleanButton;
	
	private JLabel rssNotifyLabel;
	
	public RSSPanel(Logic logic){
		this.logic = logic;
		setLayout(null);
		rssNotifyLabel = new JLabel();
		Color notifyKleur = new Color(241, 63, 83);
		rssNotifyLabel.setForeground(notifyKleur);
		rssNotifyLabel.setBounds(10, 40, 400, 22);
		
		
		rssCleanLabel = new JLabel("Verouderde data (ouder dan een jaar) verwijderen");
		rssCleanLabel.setBounds(10, 10, 300, 22);
		
		rssCleanButton = new JButton("Opschonen");
		rssCleanButton.setBounds(385, 10, 100, 22);
		rssCleanButton.addActionListener(new RSSSearchListener());
		
		add(rssNotifyLabel);
		add(rssCleanLabel);
		add(rssCleanButton);
		
	}
	
	/**
	 * Reset all form values.
	 */
	private void resetValues()
	{
		
	}
	/**
	 * Clear the notification in the notification area.
	 */
	private void clearNotification()
	{
		rssNotifyLabel.setText("");
	}
	
	/**
	 * Set a notification in the notification area
	 * @param message
	 */
	private void setNotification(String message)
	{
		rssNotifyLabel.setText(message == null ? "" : message);		
	}

	/**
	 * Private inner class to handle the search button.
	 * @author Chakir Bouziane
	 */
	private class RSSSearchListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			 logic.cleanRSSDB();
			 setNotification("Database opgeschoond");
			 resetValues();
		}
	}
	
	
}