package yarr.admin.ui;

import javax.swing.*;



import yarr.admin.domain.User;
import yarr.admin.logic.Logic;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
//import java.util.ArrayList;
//import java.util.List;



@SuppressWarnings("serial")
public class GebruikerPanel extends JPanel {
	private final Logic logic;
	private JButton userSearchButton, removeuserButton, updateuserButton;
	
	private JTextField userSearchTextField, idTextField;
	
	private JLabel userNotifyLabel, userSearchLabel, idLabel, statusLabel;
	private JLabel passwordLabel;
	private JPasswordField passwordTextField;
	private User currentUser;
//	private JComboBox statusCombo;
//	private List<String> statussen = null;
	
	
	public GebruikerPanel(Logic logic){
		this.logic = logic;
		setLayout(null);
		userNotifyLabel = new JLabel();
		Color notifyKleur = new Color(241, 63, 83);
		userNotifyLabel.setForeground(notifyKleur);
		userNotifyLabel.setBounds(10, 30, 400, 22);

		userSearchTextField = new JTextField("");
		userSearchTextField.setBounds(180, 10, 200, 22);
		
		userSearchLabel = new JLabel("Zoeken op `Email`");
		userSearchLabel.setBounds(10, 10, 200, 22);
		
		userSearchButton = new JButton("Zoeken");
		userSearchButton.setBounds(385, 10, 100, 22);
		userSearchButton.addActionListener(new UserSearchListener());
		
		idLabel = new JLabel("UserID");
		idLabel.setBounds(10, 60, 200, 22);

		idTextField = new JTextField("");
		idTextField.setBounds(180, 60, 200, 22);
		
		passwordLabel = new JLabel("Wachtwoord");
		passwordLabel.setBounds(10, 90, 200, 22);

		passwordTextField = new JPasswordField("");
		passwordTextField.setBounds(180, 90, 200, 22);
		
		statusLabel = new JLabel("Status");
		statusLabel.setBounds(10, 120, 200, 22);
		
//		statusCombo = new JComboBox();
//		statusCombo.setBounds(180, 120, 100, 22);
//		if (statussen == null)
//			statussen = logic.getStatussen();
//		List<String> items = new ArrayList<String>();
//		for (String status : statussen)
//			statusCombo.addItem(status);
		
		removeuserButton = new JButton("Verwijder gebruiker");
		removeuserButton.setBounds(180, 150, 180, 22);
		removeuserButton.addActionListener(new RemoveUserListener());
		updateuserButton = new JButton("Gebruiker opslaan");
		updateuserButton.setBounds(180, 120, 180, 22);
		updateuserButton.addActionListener(new UpdateUserListener());
		
		add(userNotifyLabel);
		add(userSearchLabel);
		add(userSearchTextField);
		add(userSearchButton);
		add(idLabel);
		add(idTextField);
		add(passwordLabel);
		add(passwordTextField);
		add(updateuserButton);
		add(removeuserButton);
//		add(statusCombo);
		
		
		
	}
	
	/**
	 * Set the current user.
	 * @param mdw
	 */
	private void setUser(User user)
	{
		userSearchTextField.setText(user.getEmail());
		idTextField.setText(Integer.toString(user.getUserId()));
		passwordTextField.setText(user.getPassword());
		
	}
	
	/**
	 * Clear the notification in the notification area.
	 */
	private void clearNotification()
	{
		userNotifyLabel.setText("");
	}
	
	/**
	 * Set a notification in the notification area
	 * @param message
	 */
	private void setNotification(String message)
	{
		userNotifyLabel.setText(message == null ? "" : message);		
	}

	private void resetValues()
	{
		userSearchTextField.setText("");
		passwordTextField.setText("");
		idTextField.setText("");		
	}
	
	/**
	 * Private inner class to handle the search button.
	 * @author Chakir Bouziane
	 */
	private class UserSearchListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			currentUser= logic.getUserByEmail(userSearchTextField.getText());
			if (currentUser == null)
			{
				setNotification("Gebruiker " + userSearchTextField.getText() + " niet gevonden");
				currentUser = null;
				resetValues();
				return;
			}
			setUser(currentUser);
			clearNotification();
		}
	}
	
	/**
	 * Private inner class to handle the removing of the user
	 * @author Chakir Bouziane
	 */
	private class RemoveUserListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			// Sanity check.
			if (currentUser == null)
			{
				setNotification("Gebruiker kan niet verwijderd worden");
				return;
			}
			//if (currentUser != null && currentUser.getId() == logic.getLoggedinUserInfo().getId())
			//{
			//	setNotification("U kunt uw eigen account niet verwijderen");
			//	return;
			//}
			// Attempt to delete the user
			try
			{
				logic.deleteUser(currentUser);
				resetValues();
				setNotification("Gebruiker verwijderd");
			}
			catch (Exception e)
			{
				setNotification("Verwijderen mislukt: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
	
	
	/**
	 * Class to handle creation / saving of  
	 * @author johan
	 */
	private class UpdateUserListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			// Attempt to create the mkantoormdw.
			try
			{
				String uid = userSearchTextField.getText();
				if (currentUser != null && uid != null && uid.length() > 0)
				{
					
					
					// Update the user information.
//					currentUser.setEmail(emailTextField.getText());
//					currentUser.setPassword(new String(passwordTextField.getPassword()));
					logic.updateUser(currentUser);
						
					
				}
			/*	else
				{
					// Check if the username exists.
					String username = mkUserTextField.getText();
					if (logic.getUserByName(username) != null)
					{
						setNotification("Gebruikersnaam " + username + " bestaat al");
						return;
					}
					
					// Create a new user.
					currentMkantoorMdw = new Mkantoormdw();
					currentMkantoorMdw.setName(mkNaamTextField.getText());
					
					// Administrator specific stuff.
					if (logic.userIsAdmin())
					{
						// Check if the mkantoor id is given.
						String mkantoorId = mkKoppelingTextField.getText();
						if (mkantoorId == null || mkantoorId.equals(""))
						{
							setNotification("Makelaarskantoor moet gegeven worden");
							return;
						}
						// Set the mkantoor id.
						currentMkantoorMdw.setMkantoor(logic.getMkantoorById(Integer.parseInt(mkantoorId)));
					}
					else
						// Add the user to the same mkantoor as the current user.
						currentMkantoorMdw.setMkantoor(securityContext.getUserInfo().getMkantoormdw().getMkantoor());
					
					// Do the actual create.
					logic.createMkantoorMdw(currentMkantoorMdw);
					
					// Create the user.
					User newUser = new User();
					newUser.setMkantoormdw(currentMkantoorMdw);
					newUser.setName(username);
					newUser.setPwd(new String(mkWWTextField.getPassword()));
					newUser.setRole(logic.getRoleByName("user"));
					logic.createUser(newUser);
					currentMkantoorMdw.setUser(newUser);
					// Refresh information.
					setMkantoorMdw(currentMkantoorMdw);
					// Notification.
					setNotification("Nieuwe makelaar aangemaakt met ID " + currentMkantoorMdw.getMkmdwid());
				}*/
			}
			catch (Exception e)
			{
				setNotification("Aanmaken/updaten mislukt: " + e.getMessage());
				e.printStackTrace();
			}
		}
	}
}
