package yarr.admin.ui;

import javax.swing.*;

import yarr.admin.logic.Logic;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

@SuppressWarnings("serial")
public class RapportenPanel extends JPanel {
	private final Logic logic;
	private JLabel userReportLabel;
	private JButton userReportButton;
	private JLabel reportsNotifyLabel;
	
	public RapportenPanel(Logic logic){
		this.logic = logic;
		setLayout(null);
		reportsNotifyLabel = new JLabel();
		Color notifyKleur = new Color(241, 63, 83);
		reportsNotifyLabel.setForeground(notifyKleur);
		reportsNotifyLabel.setBounds(10, 40, 400, 22);
		
		userReportLabel = new JLabel("Maak rapportage gebruikers");
		userReportLabel.setBounds(10, 10, 200, 22);
		
		userReportButton = new JButton("Rapport");
		userReportButton.setBounds(385, 10, 100, 22);
		userReportButton.addActionListener(new URSearchListener());
		
		add(reportsNotifyLabel);
		add(userReportLabel);
		add(userReportButton);
		
	}
	
	/**
	 * Clear the notification in the notification area.
	 */
	private void clearNotification()
	{
		reportsNotifyLabel.setText("");
	}
	
	/**
	 * Set a notification in the notification area
	 * @param message
	 */
	private void setNotification(String message)
	{
		reportsNotifyLabel.setText(message == null ? "" : message);		
	}
	
	/**
	 * Private inner class to handle the userreport button.
	 * @author Chakir Bouziane
	 */
	private class URSearchListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent arg0)
		{
			logic.getUserReport();
		}
	}
	
	
}
