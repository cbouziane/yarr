package yarr.admin.ui;

import javax.swing.*;

import yarr.admin.domain.*;
import yarr.admin.logic.Logic;

import java.util.*;


import java.awt.image.BufferedImage;
import java.awt.*;

public class MainScreen extends JFrame  {
	
	
	
	
	private BufferedImage myPicture;

	public MainScreen(Logic logic) {
		try{
		 UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
		}
		catch (Exception e){
			e.printStackTrace();
		}
		this.setTitle("Yarr admin panel");
		setSize(512, 500);
		setLocationRelativeTo(null); // Center on screen.
		setResizable(false);
		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JPanel mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		ImageIcon image = new ImageIcon("src/main/resources/logo_new.jpg");
		JLabel picLabel = new JLabel("", image, JLabel.CENTER);
		picLabel.setIcon(image);
		mainPanel.add(picLabel, BorderLayout.SOUTH);
		JLabel name = new JLabel("Welkom " + logic.getLoggedinUserInfo());
		mainPanel.add(name, BorderLayout.NORTH);
		getContentPane().add(mainPanel);

		// Create mainTabs
		JTabbedPane mainTabs = new JTabbedPane();
		mainTabs.addTab("GebruikersDatabase", new GebruikerPanel(logic));
		mainTabs.addTab("RSSDatabase", new RSSPanel(logic));
		mainTabs.addTab("Rapporten", new RapportenPanel(logic));
		mainPanel.add(mainTabs, BorderLayout.CENTER);
		mainTabs.updateUI();
		

	}
	
	
}
