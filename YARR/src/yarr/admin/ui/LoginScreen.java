
package yarr.admin.ui;

import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.*;

import yarr.admin.logic.Logic;


@SuppressWarnings("serial")
public class LoginScreen extends JFrame implements ActionListener {
	private Logic logic; 
	private MainScreen ms;	
	private JLabel usernameLabel, passwordLabel;
	private JTextField usernameField;
	private JPasswordField passwordField;
	private JButton loginButton, cancelButton;
	private boolean loggedin;
	
	public LoginScreen(Logic logic) {
		this.logic=logic;
		try{
			 UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
			}
			catch (Exception e){
				e.printStackTrace();
			}
		cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(this);
		loginButton = new JButton("Login");
		loginButton.addActionListener(this);
		usernameLabel = new JLabel("Gebruikersnaam:");	
		passwordLabel = new JLabel("Wachtwoord:");
		usernameField = new JTextField();
		passwordField = new JPasswordField();
		setLayout(new GridLayout(3, 2));
		add(usernameLabel);
		add(usernameField);
		add(passwordLabel);
		add(passwordField);
		add(loginButton);
		add(cancelButton);
		setSize(300, 125);
		setLocationRelativeTo(null);
		setVisible(true);
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		setTitle("Inloggen op YARR Admin");
	} 
	
	public void handle() {}
	
	@Override
	public void actionPerformed(ActionEvent e) {
		Object o=e.getSource();
		if (o==loginButton) {
			handleLogin();
		}
		else if (o==cancelButton) {
			dispose();
		}
	}
	
	private void handleLogin() {
		String name=usernameField.getText();
		String pwd = new String (passwordField.getPassword());
		
		if (!name.equals("") && !pwd.equals("")){
			loggedin=logic.logIn(name, pwd); 
		}

		if (!loggedin) {
			JOptionPane.showMessageDialog(this, "Inloggen niet gelukt", "", JOptionPane.ERROR_MESSAGE);
		} 
		else {
			ms = new MainScreen(logic);
			dispose();
		}
	}
}
