package yarr.admin.logic;


import java.util.List;

import yarr.admin.dao.*;
import yarr.admin.domain.*;

public class Logic {
	
	
	private final DAOFacade daoFacade;
	private String password;
	private String username;
	
	public Logic(DAOFacade daoFacade)
	{
		this.daoFacade = daoFacade;
	}

	public boolean logIn(String username, String password) {
		this.username = username;
		this.password = password;
		
		//User user=userdao.getUserByName(username);
		//if (user==null||!user.getPassword().equals(password))
		//	return false;
		return true; 
	}

	public void getUserReport() {
		// TODO Auto-generated method stub
		
	}


	public void cleanRSSDB() {
		// TODO Auto-generated method stub	
	}

	public String getLoggedinUserInfo() {
		return username;
	}

	public void deleteUser(Object currentUser) {
		// TODO Auto-generated method stub
		
	}

	public void updateUser(Object currentUser) {
		// TODO Auto-generated method stub
		
	}

	/**
	 * Get a user by email.
	 * @param email
	 * @return User
	 */
	public User getUserByEmail(String email)
	{
		return daoFacade.getUserDAO().getUserByEmail(email);
	}

	public List<String> getStatussen() {
		return null;
	}

}