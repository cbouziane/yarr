package yarr.admin.main;

import yarr.admin.dao.impl.hibernate.DAOFacadeImpl;
import yarr.admin.logic.Logic;
import yarr.admin.ui.*;

public class Main {
	private Logic logic;
	private DAOFacadeImpl daofacimpl;
	private LoginScreen login;
	
	
	public Main() {
		daofacimpl=new DAOFacadeImpl();
		logic=new Logic(daofacimpl);
		login= new LoginScreen(logic);
	}
	
	public void run() {
		login.handle(); 
	}
}
