package yarr.admin.domain;

// Generated Apr 4, 2013 2:22:50 PM by Hibernate Tools 4.0.0

import java.util.HashSet;
import java.util.Set;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import static javax.persistence.GenerationType.IDENTITY;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Proxy;

/**
 * Feed generated by hbm2java
 */
@SuppressWarnings("serial")
@Proxy(lazy=false)
@Entity
@Table(name = "feed", catalog = "yarr")
public class Feed implements java.io.Serializable {

	private Integer feedId;
	private String remoteId;
	private Set <FeedSubscription> feedSubscriptions = new HashSet<FeedSubscription>(0);
	private Set <ItemRead> itemReads = new HashSet <ItemRead>(0);

	public Feed() {
	}

	public Feed(String remoteId, Set <FeedSubscription> feedSubscriptions, Set <ItemRead> itemReads) {
		this.remoteId = remoteId;
		this.feedSubscriptions = feedSubscriptions;
		this.itemReads = itemReads;
	}

	@Id
	@GeneratedValue(strategy = IDENTITY)
	@Column(name = "feed_id", unique = true, nullable = false)
	public Integer getFeedId() {
		return this.feedId;
	}

	public void setFeedId(Integer feedId) {
		this.feedId = feedId;
	}

	@Column(name = "remote_id", length = 45)
	public String getRemoteId() {
		return this.remoteId;
	}

	public void setRemoteId(String remoteId) {
		this.remoteId = remoteId;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "feed")
	public Set <FeedSubscription> getFeedSubscriptions() {
		return this.feedSubscriptions;
	}

	public void setFeedSubscriptions(Set <FeedSubscription> feedSubscriptions) {
		this.feedSubscriptions = feedSubscriptions;
	}

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "feed")
	public Set <ItemRead> getItemReads() {
		return this.itemReads;
	}

	public void setItemReads(Set <ItemRead> itemReads) {
		this.itemReads = itemReads;
	}

}
